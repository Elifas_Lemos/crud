@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                <!--a href=""> Cadastrar Novo </a-->
                <h4>Funcionários TI SISTEMAS</h4>
                <a href="{{ url('/home') }}"> Voltar </a>
                </div>

                <!-- INCLUSÃO DAS MENSAGENS DE SUCESSO -->
                @include('includes.sucess_message')
                <!-- INCLUSÃO DAS MENSAGENS DE ERRO -->
                @include('includes.erros')

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered">
                      <thead>
                        <tr> 
                          <th scope="col">Chapa</th>
                          <th scope="col">Funcionário</th>
                          <th scope="col">Email</th>
                          <th scope="col">Função</th> 
                          <th scope="col">Setor</th> 
                        </tr>
                      </thead>
                      <tbody>
                    @foreach( $funcs as $f )
                        <tr> 
                          <td>{{ $f->chapa }}</td>
                          <td>{{ $f->nome }}</td> 
                          <td>{{ $f->email }}</td> 
                          <td>{{ $f->funcao }}</td> 
                          <td>{{ $f->setor }}</td> 
                        </tr>
                    @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
