@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                <a href="{{ url('users/new') }}"> Cadastrar Novo </a></br>
                <a href="{{ url('/home') }}"> Voltar </a>
                
                </div>

                <!-- INCLUSÃO DAS MENSAGENS DE SUCESSO -->
                @include('includes.sucess_message')
                <!-- INCLUSÃO DAS MENSAGENS DE ERRO -->
                @include('includes.erros')

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Nome</th>
                          <th scope="col">E-mail</th>
                          <th scope="col">Editar</th>
                          <th scope="col">Deletar</th>
                        </tr>
                      </thead>
                      <tbody>
                    @foreach( $usuarios as $u )

                        <tr>
                          <th scope="row">{{ $u->id }}</th>
                          <td>{{ $u->name }}</td>
                          <td>{{ $u->email }}</td>
                          <td>
                                <a href="users/{{ $u->id }}/edit" class="btn btn-info">Editar</button>
                          </td>
                          <td>
                                <form action="" method="post">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" 
                                onClick="event.preventDefault();if(confirm('Deseja excluir?')){window.location.href='users/{{$u ->id}}/delete'}"
                                >Deletar</button>
                                </form>
                          </td>
                        </tr>

                    @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
