@if(session()->has('message_error'))
    <span class="alert alert-danger"> {{ session('message_error') }} </span>
@endif