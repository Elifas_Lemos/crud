<?php

namespace App\Http\Controllers;
use App\Func;

class FuncsController extends Controller
{
    public function index(){
        $funcs  =   Func::select(['pfunc.chapa',
                                  'pfunc.nome',
                                  'ppessoa.email',
                                  'pfuncao.nome as funcao',
                                  'psecao.descricao as setor']) 
                        ->join('pfuncao','pfunc.codfuncao','pfuncao.codigo')
                        ->join('psecao','psecao.codigo', 'pfunc.codsecao')
                        ->join('ppessoa','ppessoa.codigo','pfunc.codpessoa')
                        ->where([
                                ['pfunc.codsecao','=', '01.002.06.02'],
                                ['pfunc.codsituacao','=','A']])
                                ->orderBy('pfunc.chapa', 'asc')
                                ->get();

        //$funcs = new Func();
        //$funcs->setConnection('oracle');
        
        //$funcs = Func::whereIN('chapa',[4037,4038,4093])->orderBy('nome', 'asc')->get();

        return view('funcs.listFuncs',['funcs' => $funcs]);

    }

}
