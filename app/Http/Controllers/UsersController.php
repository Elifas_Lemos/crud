<?php

namespace App\Http\Controllers;
use App\Usuario;
use App\Http\Requests\CreateUsersRequest;

/*
PARABENS! FICOU MUITO BOM. NOTE ABAICO MINHAS CONSIDERAÇÕES
*/
class UsersController extends Controller
{
    public function index(){
        //estude sobre o seguinte assunto: injeção de dependência
        $usuarios  =  Usuario::get();

        return view('users.listUsers',['usuarios' => $usuarios]);
    }

    public function new(){

        return view('users.form');
    }


    public function add(CreateUsersRequest $request){

        try {

            $usuario =  new Usuario;
            $usuario = $usuario->create( $request->all() );

            session()->flash('message', 'Usuário cadastrado com sucesso!');

            //return Redirect::to('/users');
            //Não precisa chamar novamente a Redirect. O Laravel já controla isso para você. Use conforme abaixo.
            // Observe que no arquivo de rotas "web" tive que adiconar o nome da rota. Note linha 25 do arquivo web em routes.
            return redirect()->route("users");

        } catch (\Throwable $th) {
            session()->flash('message_error', 'Usuário não cadastrado!');

            //return view('users.form');
            //O ideal é que você redirecione para rotas já existentes
            // Observe que no arquivo de rotas "web" tive que adiconar o nome da rota. Note linha 25 do arquivo web em routes.
            return redirect()->route("users");

        }
    }

    public function edit($id){
        try {

            //estude sobre o seguinte assunto: injeção de dependência
            $usuario = Usuario::findOrFail($id);

            //return view('users.form',['usuario' => $usuario]);
            //O Laravel tem um facilitador para passagem de parametros.
            //Use "compact" passando o nome da variavel. Da o mesmo que usar como você fez acima.
            return view('users.form',compact("usuario"));

        } catch (\Throwable $th) {
            //Nunca deixe o catch vazio
            //throw $th;
        }

    }

    public function update($id,CreateUsersRequest $request){
        try {

            //estude sobre o seguinte assunto: injeção de dependência
            $usuario = Usuario::findOrFail($id);
            $usuario = $usuario->update( $request->all() );

            session()->flash('message', 'Usuário atualizado com sucesso!');

            //Não precisa chamar novamente a Redirect. O Laravel já controla isso para você. Use conforme abaixo.
            // Observe que no arquivo de rotas "web" deve-se adiconar o nome da rota. Note linha 25 do arquivo web em routes.
            return redirect()->route("users");

        } catch (\Throwable $th) {
            //Nunca deixe o catch vazio
            //throw $th;
        }
    }


    public function delete($id){
        try {

            //estude sobre o seguinte assunto: injeção de dependência
            $usuario = Usuario::findOrFail($id);
            $usuario->delete();

            session()->flash('message', 'Usuário deletado com sucesso!');

            //Não precisa chamar novamente a Redirect. O Laravel já controla isso para você. Use conforme abaixo.
            // Observe que no arquivo de rotas "web" deve-se adiconar o nome da rota. Note linha 25 do arquivo web em routes.
            return redirect()->route("users");

        } catch (\Throwable $th) {
            //Nunca deixe o catch vazio
            //throw $th;
        }

    }
}
