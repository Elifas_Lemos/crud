<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
/*ROTAS DE NAVEGAÇÃO DO USUÁRIO*/
Route::get('/funcs', 'FuncsController@index')->middleware('auth')->name("funcs");
Route::get('/users', 'UsersController@index')->middleware('auth')->name("users");
Route::get('/users/new', 'UsersController@new')->middleware('auth');
Route::post('users/add', 'UsersController@add')->middleware('auth');
Route::get('users/{id}/edit', 'UsersController@edit')->middleware('auth');
Route::post('/users/update/{id}', 'UsersController@update')->middleware('auth');
Route::delete('/users/delete/{id}', 'UsersController@delete')->middleware('auth');
Route::get('users/{id}/delete', 'UsersController@delete')->middleware('auth');




